#!/usr/bin/env python3
# vim: filetype=python

import facebook
import os
from pathlib import Path
from random import randrange
import requests
import schedule
import time
import yaml

def load_config():
    with open('./token.yml') as stream:
        try:
            CONFIG = yaml.safe_load(stream)
        except Exception as err:
            raise err
        if not CONFIG['FB_TOKEN']:
            raise Exception('You forgot the token dummy,,\
                            check the token.yml file again 😮 ')
        else:
            os.environ['token'] = CONFIG['FB_TOKEN']

def send_post(author='nil', image=''):
    graph.put_object(
        # The page which we're posting to
        fb_page_id,
        # Where we're posting to
        'feed',
        # The post we're making - as a test just print this dumb shit.
        message=(f'this was by {author}', image)
    )

if __name__ == '__main__':
    # Load all our env vars from the config
    load_config()
    # Now set the things we need
    page_id_1 = 'WaifuDynamics'
    fb_token_1 = os.environ['token']
    graph = facebook.GraphAPI(fb_token_1)
    fb_page_id='WaifuDynamics'
    img_url = 'https://graph.facebook.com/{}/photos'.format(page_id_1)

    # We could use a generator here but a) I'm too lazy b) I'm too drunk c) this works.
    img_arr = []
    for image in Path('./img/').glob('*.webp'):
        img_arr.append(image)

    # The actual post we want to make.
    post = str(img_arr[randrange(len(img_arr))]).split('img/')[1]

    # And your artist credit
    author = str(post.split('-')[0].split('.')[0])
    print(post, author)

    # First for the morn
    schedule.every().day.at('09:20').do(send_post(author, post))

    # And for the eve'
    schedule.every().day.at('18:30').do(send_post(author, post))

    # Then just post it for ages.
    while 1:
        schedule.run_pending()
        time.sleep(1)

