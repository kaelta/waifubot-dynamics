{ pkgs ? import (fetchTarball https://github.com/nixos/nixpkgs/archive/nixpkgs-unstable.tar.gz) {} }:

let
  venvPython = pkgs.python38.buildEnv.override {
    extraLibs = [ pkgs.python38Packages.virtualenv ];
  };
in
  pkgs.mkShell {
    buildInputs = [ venvPython ];
}
