from setuptools import setup

setup(
   name='waifudyn_bot',
   version='1.0',
   description='Something different',
   author='Kae',
   author_email='kaedoesnotexist@nxdomain.net',
   packages=['waifudyn_bot'],  # would be the same as name
   install_requires=['virtualenv'], #external packages acting as dependencies
   scripts=[
       'waifudyn_bot/waifudyn_bot.py',
    ]
)
